# Word Permutation (Bingham University Hackathon 2018)

####  Problem Set
> Create a permutation web application.

####  Description

A very basic web based application that determines the possible outcome of a word a user enters.
> E.g input = <code>abcde</code> => possible outccomes = 325

#### PHOTO
![alt text](https://github.com/int-elligentSam/word_permutation/blob/master/static/img/permutation_photo.gif "Image here")

#### ToolSet 🏗
1. Python
2. Django (Python Web Framework)
3. JQuery

# TEAM 👏👏👏👏
Shout out to my awesome team ✌ members @:
1. Timi
2. Joel
3. Joshua
4. Marvelous
5. Others...

# 🤓😜
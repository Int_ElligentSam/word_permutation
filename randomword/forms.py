from django import forms


class PermutationForm(forms.Form):
    # word permutation form
    word = forms.CharField(label="Enter word", max_length=255, required=True)
from django.shortcuts import render
from .forms import PermutationForm
from itertools import permutations, combinations
import string

# TODO // algorithm is slow (Improve on performance)
def perform_calculation(word):
    main_list = []
    length = range(len(word))
    for i in length:
        # TODO // implement custom/self permutation algorithm
        main_list += list(permutations(word, i+1))
    return main_list

"""
@ Depricated function:: Just for recall.
========================================
# def swapitem(first, last):
#     if len(first) > len(last):
#         return first
#     return last
"""

def form_view(request):
    # display on get request
    if request.method == 'GET':
        form = PermutationForm()
    # display on post request
    elif request.method == 'POST':
        form = PermutationForm(request.POST)
        if form.is_valid():
            word = form.cleaned_data['word']
            word =  sorted(perform_calculation(word))
            return render(request, 'form.html', {'form': form, 'word': word, 'count': len(word)})
    return render(request, 'form.html', {'form': form})
